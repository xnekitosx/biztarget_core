

    var qqq = function () {
		
    	var self = this;
		var system = self.system;
		

		this.log = function (str) {
			console.log(str);
			return true;
		}
		

		this.callbacks = {
			
			render: function() {
				self.log("render qqq");

				
				self.log("render end");
				return true;
			},
			
			
			init: function(){
				self.log("init start qqq");

				self.log("init stop");
				return true;
			},
			
			
			bind_actions: function() {
				
				self.log("bind_actions start qqq");
				
				
				self.log("bind_actions stop");
				return true;
			},
			
			
			settings: function(){

				return true;
			},
			
			
			onSave: function(){

				return true;
			},
			
			
			destroy: function(){

			},
			
			
			contacts: {
					selected: function(){

					}
				},
				
				
			leads: {
					selected: function(){
					}
				},
				
				
			tasks: {
					selected: function(){
					}
				}
				
		};
		return this;
    };
	
	var q = new qqq();
	window.myWidget.init.push(q.callbacks['init']);
	window.myWidget.render.push(q.callbacks['render']);
	window.myWidget.bind_actions.push(q.callbacks['bind_actions']);
	window.myWidget.settings.push(q.callbacks['settings']);
	window.myWidget.onSave.push(q.callbacks['onSave']);
	window.myWidget.destroy.push(q.callbacks['destroy']);
	window.myWidget.contacts.push(q.callbacks['contacts']);
	window.myWidget.leads.push(q.callbacks['leads']);
	window.myWidget.tasks.push(q.callbacks['tasks']);

	