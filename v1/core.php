<?php

	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);

	header('Access-Control-Allow-Origin: *');
	header('Cache-Control: no-cache, must-revalidate');
	header('Pragma: no-cache');
	
	
class BizCore
{
	
	protected $config;
	protected $data;
	protected $action;
	protected $is_action;
	protected $is_error;
	protected $ans;
	protected $success;
	protected $time;
	protected $code;
	
	public function __construct ( $config ) 
	{
		$this->setConfig( $config );
		$this->action = null;
		$this->is_action = false;
		$this->is_error = false;
		$this->ans = [];
		$this->success = false;
		$this->code = 0;
		$this->execAction();	
		$this->time = microtime(true);		
	}
	
	public function __destruct()
	{
		$this->ans['time'] = round(microtime(true) - $this->time, 3);
		$this->ans['_success'] = $this->success;
		$this->ans['_action'] = $this->action;
		echo json_encode($this->ans);
	}
	
	public function setConfig( $config ) 
	{
		$this->config = $config;
	}
	
	public function getConfig() 
	{
		return $this->config;
	}
	
	public function param( $key = null ) 
	{
		if ( $key === null ) {
			return $this->data;
		} else {
			if ( isset( $this->data[ $key ] ) ) {
				return $this->data[ $key ];
			} else {
				return null;
			}
			
		}		
	}
	
	public function send( $key = null, $value = null ) 
	{
		if ( $this->is_error == false ) {
			$this->success = true;
		}
		
		if ( $value === null && $key === null ) {
			return $this->ans;
		} else {
			if ( $value == null && $key !== null ) {
				if ( is_array( $key ) == true ) {
					$this->ans = array_merge($this->ans, $key);
				} else {
					$this->ans[] = $key;
				}	
			} else {
				$this->ans[ $key ] = $value;
			}
		}
	}
	
	public function remove( $key = null ) 
	{
		if ( isset( $key ) ) {
			$arr = $this->ans[ $key ];
			unset( $this->ans[ $key ] );
			return $arr;
		} else {
			$arr = $this->ans;
			$this->ans = [];
			return $arr;
		}
	}
	
	public function error()
	{
		$this->success = false;
		$this->is_error = true;
	}
	
	public function success()
	{
		$this->success = true;
		$this->is_error = false;
	}
	
	public function getAction() {
		return $this->action;
	}
	
	private function execAction() 
	{
		$this->data = array_merge($_GET, $_POST);
		$this->is_action = isset($this->data["_action"]) && !empty($this->data["_action"]);
		if ( $this->is_action ) {
			$this->action = $this->data["_action"];
		} else {
			$this->success = false;
		}
	}
	
	public function query( $query = null, $data = null, $date = null )
	{

		if ( is_null( $query ) ) {
			return null;
		}
		
		if ( strripos($query, "?") === false ) {
			$query .= "?";
		} else {
			$query .= "&";
		}
		
		/*if ( is_null( $data ) ) {
			$query .= "USER_LOGIN=".$this->config['amo_login']."&USER_HASH=".$this->config['amo_hash'];
		}*/
		
		$link = 'https://' . $this->config['amo_domain'] .  $query;
		
		/* создаем буфер, чтобы лишнего не выводить */
		ob_start();
		/* запрос curl по документации. */
		$curl=curl_init();
		curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
		curl_setopt($curl, CURLOPT_URL, $link);
		/* если GET запрос, то параметры ниже не нужны */
		if ( !is_null( $data ) ) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		} else {
			if ( !is_null( $date ) ) {
				$date = date("D, d M Y H:i:s", $date);
				curl_setopt($curl,CURLOPT_HTTPHEADER,array('IF-MODIFIED-SINCE: '.$date));
			}
		}
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__).'/cookie/'.$this->config['amo_domain'].'.txt');
		curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie/'.$this->config['amo_domain'].'.txt');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		$out=curl_exec($curl); 
		$out2 = ob_GET_contents();
		ob_end_clean ();
		$code=curl_GETinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		
		if ( $code == 401 ) {
			if ( $this->code == 401 ) {
				return;
			}
			sleep(1);
			$this->code = $code;
			$this->query('/private/api/auth.php?type=json', [
				'USER_LOGIN'	=>	$this->config['amo_login'],
				'USER_HASH'	=>	$this->config['amo_hash'],
			]);
			return $this->query( $query, $data, $date);
		}
		
		
		return json_decode($out2,true);
		
	}
	
}
	
