    var zzz = function () {
		
    	var self = this;
		var system = self.system;
		

		this.log = function (str) {
			console.log(str);
			return true;
		}
		

		this.callbacks = {
			
			render: function() {
				self.log("render zzz");

				
				self.log("render end");
				return true;
			},
			
			
			init: function(){
				self.log("init start zzz");

				self.log("init stop");
				return true;
			},
			
			
			bind_actions: function() {
				
				self.log("bind_actions start zzz");
				
				
				self.log("bind_actions stop);
				return true;
			},
			
			
			settings: function(){

				return true;
			},
			
			
			onSave: function(){

				return true;
			},
			
			
			destroy: function(){

			},
			
			
			contacts: {
					selected: function(){

					}
				},
				
				
			leads: {
					selected: function(){
					}
				},
				
				
			tasks: {
					selected: function(){
					}
				}
				
		};
		return this;
    };
	
	var z = new zzz();
	window.myWidget.init.push(z.callbacks['init']);
	window.myWidget.render.push(z.callbacks['render']);
	window.myWidget.bind_actions.push(z.callbacks['bind_actions']);
	window.myWidget.settings.push(z.callbacks['settings']);
	window.myWidget.onSave.push(z.callbacks['onSave']);
	window.myWidget.destroy.push(z.callbacks['destroy']);
	window.myWidget.contacts.push(z.callbacks['contacts']);
	window.myWidget.leads.push(z.callbacks['leads']);
	window.myWidget.tasks.push(z.callbacks['tasks']);
